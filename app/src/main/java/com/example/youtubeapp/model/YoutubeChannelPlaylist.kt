package com.example.youtubeapp.model

import com.google.gson.annotations.Expose

/** Rae-An Andres created on 2020-06-02*/

data class YoutubeChannelPlaylist(val category: String,
                                  val title: String,
                                  val description: String,
                                  val publishedAt: String,
                                  val thumbNail: String)


//data class YoutubeSearchResponse(@Expose val kind: String, @Expose val etag: String, @Expose val nextPageToken)