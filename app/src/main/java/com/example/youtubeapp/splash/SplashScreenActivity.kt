package com.example.youtubeapp.splash

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.example.youtubeapp.R
import com.example.youtubeapp.common.YoutubeApplication
import com.example.youtubeapp.common.preference.SharedPrefUtil
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.common.view.BaseActivity
import com.example.youtubeapp.googleauth.GoogleSignInActivity
import com.example.youtubeapp.youtubepage.YoutubePageActivity
import com.google.android.gms.security.ProviderInstaller
import com.google.gson.reflect.TypeToken

/** Rae-An Andres created on 2020-06-02*/

class SplashScreenActivity : BaseActivity() {

    private lateinit var relativeLayout: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_splash_screen)

        relativeLayout = findViewById(R.id.splash_layout)

        decorateProgressLoader(relativeLayout)

    }


    override fun onResume() {
        super.onResume()


        progressBar.visibility = View.VISIBLE

        Handler().postDelayed(Runnable {

            checkPermission(this, hasPermission = {

                    hasPermission ->

                if (!hasPermission) {

                    askForPermission(this)

                } else {
                    checkPreviousLogins()
                }

            })

        },1000)



    }

    private fun checkPreviousLogins() {

        val sharedPref = SharedPrefUtil.init(this)

        val channelId = sharedPref.getChannelIdFromPrefs()

        sharedPref.getRelatedPlaylistFromShared() ?.let {
            playlist ->
            Log.d("YOUTUBE", "Youtube Stored Playlist: $playlist")
        }



        progressBar.visibility = View.GONE

        channelId?.let {

            channelId ->

            YoutubePageActivity.navigate(this, channelId)
            finish()


        } ?: run {


            GoogleSignInActivity.navigate(this)
            finish()
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        evaluateOnActivityResult(this,requestCode,resultCode,data, executeAction = {})
    }

    override fun onPostResume() {
        super.onPostResume()

        if (YoutubeApplication.INSTANCE.isRetryProviderInstall()) {
            // We can now safely retry installation.
            ProviderInstaller.installIfNeededAsync(this, YoutubeApplication.INSTANCE)
        }

        YoutubeApplication.INSTANCE.setRetryProviderInstall(false)
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when(requestCode) {
            permissionRequestCode -> {
                var allGranted = false

                for (grantResult in grantResults) {

                    if (grantResult == PackageManager.PERMISSION_GRANTED) {

                        allGranted = true
                    } else {
                        allGranted = false
                        break
                    }

                }

                if (allGranted) {

                    checkPreviousLogins()

                } else {
                    finish()
                }
            }
        }


    }
}