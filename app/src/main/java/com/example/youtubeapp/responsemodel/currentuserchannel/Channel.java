package com.example.youtubeapp.responsemodel.currentuserchannel;

import com.google.gson.annotations.SerializedName;

public class Channel{

	@SerializedName("showRelatedChannels")
	private boolean showRelatedChannels;

	@SerializedName("title")
	private String title;

	@SerializedName("profileColor")
	private String profileColor;

	public void setShowRelatedChannels(boolean showRelatedChannels){
		this.showRelatedChannels = showRelatedChannels;
	}

	public boolean isShowRelatedChannels(){
		return showRelatedChannels;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setProfileColor(String profileColor){
		this.profileColor = profileColor;
	}

	public String getProfileColor(){
		return profileColor;
	}

	@Override
 	public String toString(){
		return 
			"Channel{" + 
			"showRelatedChannels = '" + showRelatedChannels + '\'' + 
			",title = '" + title + '\'' + 
			",profileColor = '" + profileColor + '\'' + 
			"}";
		}
}