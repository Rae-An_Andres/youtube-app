package com.example.youtubeapp.responsemodel.videodetails;

import com.google.gson.annotations.SerializedName;

public class ContentDetails{

	@SerializedName("duration")
	private String duration;

	@SerializedName("licensedContent")
	private boolean licensedContent;

	@SerializedName("caption")
	private String caption;

	@SerializedName("definition")
	private String definition;

	@SerializedName("contentRating")
	private ContentRating contentRating;

	@SerializedName("projection")
	private String projection;

	@SerializedName("dimension")
	private String dimension;

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setLicensedContent(boolean licensedContent){
		this.licensedContent = licensedContent;
	}

	public boolean isLicensedContent(){
		return licensedContent;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public String getCaption(){
		return caption;
	}

	public void setDefinition(String definition){
		this.definition = definition;
	}

	public String getDefinition(){
		return definition;
	}

	public void setContentRating(ContentRating contentRating){
		this.contentRating = contentRating;
	}

	public ContentRating getContentRating(){
		return contentRating;
	}

	public void setProjection(String projection){
		this.projection = projection;
	}

	public String getProjection(){
		return projection;
	}

	public void setDimension(String dimension){
		this.dimension = dimension;
	}

	public String getDimension(){
		return dimension;
	}

	@Override
 	public String toString(){
		return 
			"ContentDetails{" + 
			"duration = '" + duration + '\'' + 
			",licensedContent = '" + licensedContent + '\'' + 
			",caption = '" + caption + '\'' + 
			",definition = '" + definition + '\'' + 
			",contentRating = '" + contentRating + '\'' + 
			",projection = '" + projection + '\'' + 
			",dimension = '" + dimension + '\'' + 
			"}";
		}
}