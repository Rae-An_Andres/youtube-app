package com.example.youtubeapp.responsemodel.currentuserchannel;

import com.google.gson.annotations.SerializedName;

public class RelatedPlaylists{

	@SerializedName("favorites")
	private String favorites;

	@SerializedName("watchHistory")
	private String watchHistory;

	@SerializedName("watchLater")
	private String watchLater;

	@SerializedName("likes")
	private String likes;

	@SerializedName("uploads")
	private String uploads;

	public void setFavorites(String favorites){
		this.favorites = favorites;
	}

	public String getFavorites(){
		return favorites;
	}

	public void setWatchHistory(String watchHistory){
		this.watchHistory = watchHistory;
	}

	public String getWatchHistory(){
		return watchHistory;
	}

	public void setWatchLater(String watchLater){
		this.watchLater = watchLater;
	}

	public String getWatchLater(){
		return watchLater;
	}

	public void setLikes(String likes){
		this.likes = likes;
	}

	public String getLikes(){
		return likes;
	}

	public void setUploads(String uploads){
		this.uploads = uploads;
	}

	public String getUploads(){
		return uploads;
	}

	@Override
 	public String toString(){
		return 
			"RelatedPlaylists{" + 
			"favorites = '" + favorites + '\'' + 
			",watchHistory = '" + watchHistory + '\'' + 
			",watchLater = '" + watchLater + '\'' + 
			",likes = '" + likes + '\'' + 
			",uploads = '" + uploads + '\'' + 
			"}";
		}
}