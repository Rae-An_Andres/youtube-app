package com.example.youtubeapp.responsemodel.currentuserchannel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BrandingSettings{

	@SerializedName("image")
	private Image image;

	@SerializedName("hints")
	private List<HintsItem> hints;

	@SerializedName("channel")
	private Channel channel;

	public void setImage(Image image){
		this.image = image;
	}

	public Image getImage(){
		return image;
	}

	public void setHints(List<HintsItem> hints){
		this.hints = hints;
	}

	public List<HintsItem> getHints(){
		return hints;
	}

	public void setChannel(Channel channel){
		this.channel = channel;
	}

	public Channel getChannel(){
		return channel;
	}

	@Override
 	public String toString(){
		return 
			"BrandingSettings{" + 
			"image = '" + image + '\'' + 
			",hints = '" + hints + '\'' + 
			",channel = '" + channel + '\'' + 
			"}";
		}
}