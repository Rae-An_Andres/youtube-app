package com.example.youtubeapp.responsemodel.currentuserchannel;

import com.google.gson.annotations.SerializedName;

public class ContentDetails{

	@SerializedName("relatedPlaylists")
	private RelatedPlaylists relatedPlaylists;

	public void setRelatedPlaylists(RelatedPlaylists relatedPlaylists){
		this.relatedPlaylists = relatedPlaylists;
	}

	public RelatedPlaylists getRelatedPlaylists(){
		return relatedPlaylists;
	}

	@Override
 	public String toString(){
		return 
			"ContentDetails{" + 
			"relatedPlaylists = '" + relatedPlaylists + '\'' + 
			"}";
		}
}