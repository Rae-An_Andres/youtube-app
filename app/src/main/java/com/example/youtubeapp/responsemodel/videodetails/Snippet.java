package com.example.youtubeapp.responsemodel.videodetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Snippet{

	@SerializedName("defaultLanguage")
	private String defaultLanguage;

	@SerializedName("publishedAt")
	private String publishedAt;

	@SerializedName("defaultAudioLanguage")
	private String defaultAudioLanguage;

	@SerializedName("localized")
	private Localized localized;

	@SerializedName("description")
	private String description;

	@SerializedName("title")
	private String title;

	@SerializedName("thumbnails")
	private Thumbnails thumbnails;

	@SerializedName("channelId")
	private String channelId;

	@SerializedName("categoryId")
	private String categoryId;

	@SerializedName("channelTitle")
	private String channelTitle;

	@SerializedName("tags")
	private List<String> tags;

	@SerializedName("liveBroadcastContent")
	private String liveBroadcastContent;

	public void setDefaultLanguage(String defaultLanguage){
		this.defaultLanguage = defaultLanguage;
	}

	public String getDefaultLanguage(){
		return defaultLanguage;
	}

	public void setPublishedAt(String publishedAt){
		this.publishedAt = publishedAt;
	}

	public String getPublishedAt(){
		return publishedAt;
	}

	public void setDefaultAudioLanguage(String defaultAudioLanguage){
		this.defaultAudioLanguage = defaultAudioLanguage;
	}

	public String getDefaultAudioLanguage(){
		return defaultAudioLanguage;
	}

	public void setLocalized(Localized localized){
		this.localized = localized;
	}

	public Localized getLocalized(){
		return localized;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setThumbnails(Thumbnails thumbnails){
		this.thumbnails = thumbnails;
	}

	public Thumbnails getThumbnails(){
		return thumbnails;
	}

	public void setChannelId(String channelId){
		this.channelId = channelId;
	}

	public String getChannelId(){
		return channelId;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setChannelTitle(String channelTitle){
		this.channelTitle = channelTitle;
	}

	public String getChannelTitle(){
		return channelTitle;
	}

	public void setTags(List<String> tags){
		this.tags = tags;
	}

	public List<String> getTags(){
		return tags;
	}

	public void setLiveBroadcastContent(String liveBroadcastContent){
		this.liveBroadcastContent = liveBroadcastContent;
	}

	public String getLiveBroadcastContent(){
		return liveBroadcastContent;
	}

	@Override
 	public String toString(){
		return 
			"Snippet{" + 
			"defaultLanguage = '" + defaultLanguage + '\'' + 
			",publishedAt = '" + publishedAt + '\'' + 
			",defaultAudioLanguage = '" + defaultAudioLanguage + '\'' + 
			",localized = '" + localized + '\'' + 
			",description = '" + description + '\'' + 
			",title = '" + title + '\'' + 
			",thumbnails = '" + thumbnails + '\'' + 
			",channelId = '" + channelId + '\'' + 
			",categoryId = '" + categoryId + '\'' + 
			",channelTitle = '" + channelTitle + '\'' + 
			",tags = '" + tags + '\'' + 
			",liveBroadcastContent = '" + liveBroadcastContent + '\'' + 
			"}";
		}
}