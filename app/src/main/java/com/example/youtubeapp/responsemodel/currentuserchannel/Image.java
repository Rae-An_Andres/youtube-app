package com.example.youtubeapp.responsemodel.currentuserchannel;

import com.google.gson.annotations.SerializedName;

public class Image{

	@SerializedName("bannerImageUrl")
	private String bannerImageUrl;

	public void setBannerImageUrl(String bannerImageUrl){
		this.bannerImageUrl = bannerImageUrl;
	}

	public String getBannerImageUrl(){
		return bannerImageUrl;
	}

	@Override
 	public String toString(){
		return 
			"Image{" + 
			"bannerImageUrl = '" + bannerImageUrl + '\'' + 
			"}";
		}
}