package com.example.youtubeapp.responsemodel.currentuserchannel;

import com.google.gson.annotations.SerializedName;

public class HintsItem{

	@SerializedName("property")
	private String property;

	@SerializedName("value")
	private String value;

	public void setProperty(String property){
		this.property = property;
	}

	public String getProperty(){
		return property;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"HintsItem{" + 
			"property = '" + property + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}