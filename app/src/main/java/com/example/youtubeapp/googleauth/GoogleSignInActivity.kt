package com.example.youtubeapp.googleauth

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import com.example.youtubeapp.R
import com.example.youtubeapp.common.network.ApiCall
import com.example.youtubeapp.common.view.BaseActivity
import com.example.youtubeapp.youtubepage.YoutubePageActivity

/** Rae-An Andres created on 2020-06-02*/


class GoogleSignInActivity : BaseActivity() {


    companion object {
        fun navigate(callingActivity: Activity) {
            val intent = Intent(callingActivity, GoogleSignInActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
            }
            callingActivity.startActivity(intent)
        }
    }

    private lateinit var signInButton: Button

    private lateinit var relativeLayout : RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_google_sign_in)

        relativeLayout = findViewById(R.id.google_sign_in_layout)

        decorateProgressLoader(relativeLayout)

        progressBar.visibility = View.GONE

        // sign in flow

        signInButton = findViewById(R.id.sign_in_button) // implement Google Sign In UI


    }

    override fun onResume() {
        super.onResume()

        signInButton.setOnClickListener {

            // implement login
            getResultsFromApi(this, completion = {

                // implement youtube api call

                ApiCall.INSTANCE.getCurrentUserChannels(this, credential,

                    showLoading = {
                            showLoading ->

                        if (showLoading) {
                            progressBar.visibility = View.VISIBLE
                        } else {
                            progressBar.visibility = View.GONE
                        }
                    },

                    completion = { response ->

                        response?.let {
                                data ->

                            Log.d("YOUTUBE","Youtube Channels: $response")

                            val channelId = data.items[0].id


                            YoutubePageActivity.navigate(this, channelId)
                            finish()


                        }

                    })

            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        evaluateOnActivityResult(this,requestCode,resultCode,data, executeAction = {
            signInButton.callOnClick()
        })

    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


}
