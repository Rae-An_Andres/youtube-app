package com.example.youtubeapp.common.view

import android.Manifest
import android.R
import android.accounts.AccountManager
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.youtubeapp.common.REQUEST_ACCOUNT_PICKER
import com.example.youtubeapp.common.REQUEST_AUTHORIZATION
import com.example.youtubeapp.common.REQUEST_GOOGLE_PLAY_SERVICES
import com.example.youtubeapp.common.YoutubeApplication
import com.example.youtubeapp.common.preference.SharedPrefUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.youtube.YouTubeScopes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

/** Rae-An Andres created on 2020-06-02*/


open class BaseActivity : AppCompatActivity() {

    val permissionList =
        arrayOf( Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE ,
            Manifest.permission.GET_ACCOUNTS)

    private lateinit var mFragmentManager : FragmentManager

    protected lateinit var googleSignInClient : GoogleSignInClient

    protected lateinit var progressBar: ProgressBar

    val permissionRequestCode = 1000

    val SCOPES = mutableListOf(YouTubeScopes.YOUTUBE_READONLY)

    lateinit var credential: GoogleAccountCredential

    val uiScope by lazy {
        CoroutineScope(Dispatchers.Main + Job())
    }

    val ioScope by lazy {
        CoroutineScope(Dispatchers.IO + Job())
    }

    val defaultScope by lazy {
        CoroutineScope(Dispatchers.Default + Job())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFragmentManager = supportFragmentManager



        //Check google API if still signed in
        /*****************************************************************/ // check if user has already signed in to google
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        // Initialize credentials and service object.
        credential = GoogleAccountCredential.usingOAuth2(
            applicationContext, SCOPES)
            .setBackOff(ExponentialBackOff())

    }

    fun addFragment(id: Int, fragment: Fragment?): FragmentTransaction? {
        try {
            return mFragmentManager.beginTransaction()
                .add(id,fragment!!)
        } catch (stateException: IllegalStateException) {
            Log.e("FragmentManager", "exception: ${stateException.message}")
        }
        return null
    }

    fun replaceFragment(id: Int, fragment: Fragment?): FragmentTransaction? {
        try {
            return mFragmentManager.beginTransaction()
                .replace(id,fragment!!)
        } catch (stateException: IllegalStateException) {
            Log.e("FragmentManager", "exception: ${stateException.message}")
        }
        return null
    }

    fun removeFragment(fragment: Fragment?): FragmentTransaction? {
        try {
            return mFragmentManager.beginTransaction()
                .remove(fragment!!)
        } catch (stateException: IllegalStateException) {
            Log.e("FragmentManager", "exception: ${stateException.message}")
        }
        return null
    }

    protected fun decorateProgressLoader(layout: RelativeLayout) {
        progressBar = ProgressBar(
            this,
            null,
            R.attr.progressBarStyleLarge
        )
        val params = RelativeLayout.LayoutParams(300, 300)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        layout.addView(progressBar, params)


        progressBar.visibility = View.VISIBLE
    }


    inline fun checkPermission(activity: Activity, crossinline hasPermission: (Boolean) -> Unit) {

        // check if permission not granted
        if (!checkSelfPermission(activity)) {
            // suspend the system by 1 second
            Handler().postDelayed( {

                if (checkSelfPermission(activity)) {

                    hasPermission.invoke(true)
                } else {
                    hasPermission.invoke(false)

                }

            }, 1000)

        } else {
            // permission has already been granted, proceed to request API
            hasPermission.invoke(true)
        }
    }

    inline fun getResultsFromApi(activity: Activity, crossinline completion: () -> Unit) {

        when {

            credential.selectedAccountName == null -> {

                checkAccount(activity)

            }
            !isDeviceOnline() -> {

                Toast.makeText(activity,"No Internet Access Available.",Toast.LENGTH_SHORT).show()
            }
            else -> {

                completion.invoke()
            }

        }

    }

    fun getResultsFromApi(activity: Activity) {

        //overload function to optimize function that doesn't need the completion callback
        getResultsFromApi(activity,completion = {})
    }


     fun checkAccount(activity: Activity) {
        // check accounts

        checkPermission(this, hasPermission = { hasPermission ->


            if (hasPermission) {

                val prefs = SharedPrefUtil.init(activity)

                prefs.getUserUserNameFromPrefs()?.let {
                   accountName ->
                   credential.selectedAccountName = accountName

                   getResultsFromApi(activity)

               } ?: run {
                   // no account yet; start a dialog from which a user can choose an account
                   activity.startActivityForResult(
                       credential.newChooseAccountIntent(),
                       REQUEST_ACCOUNT_PICKER
                   )

               }

            } else {
                // no permission yet; request permission
                askForPermission(this)
            }

        })
    }

    fun checkSelfPermission(activity: Activity) : Boolean {


        var allGranted = false

        for(permission in permissionList) {

            if (ContextCompat.checkSelfPermission(activity,permission) == PackageManager.PERMISSION_GRANTED) {

                allGranted = true

            } else {

                allGranted = false
                break
            }

        }

        return allGranted

    }

    fun askForPermission(activity: Activity) {

        ActivityCompat.requestPermissions(activity, permissionList,permissionRequestCode)

    }

    fun isGooglePlayServicesAvailable(): Boolean {
        val apiAvailability: GoogleApiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode: Int = apiAvailability.isGooglePlayServicesAvailable(this)
        return connectionStatusCode == ConnectionResult.SUCCESS
    }


    fun acquireGooglePlayServices() {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode,this)
        }
    }


    fun showGooglePlayServicesAvailabilityErrorDialog(
        connectionStatusCode: Int, activity: Activity
    ) {
        val apiAvailability = GoogleApiAvailability.getInstance()
       val dialog = apiAvailability.getErrorDialog(
            activity,
            connectionStatusCode,
            REQUEST_GOOGLE_PLAY_SERVICES
        )
        dialog.show()


    }

    override fun onStop() {
        super.onStop()

    }

    override fun onDestroy() {
        super.onDestroy()

    }


    fun isDeviceOnline(): Boolean {
        val connMgr: ConnectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun evaluateOnActivityResult(activity: Activity, requestCode: Int,resultCode: Int,data: Intent?, executeAction: () -> Unit) {

        when (requestCode) {
            YoutubeApplication.ERROR_DIALOG_REQUEST_CODE -> {
                // Adding a fragment via GoogleApiAvailability.showErrorDialogFragment
                // before the instance state is restored throws an error. So instead,
                // set a flag here, which will cause the fragment to delay until
                // onPostResume.

                YoutubeApplication.INSTANCE.setRetryProviderInstall(true)
                return
            }

            REQUEST_GOOGLE_PLAY_SERVICES -> {

                if (resultCode != Activity.RESULT_OK) {

                    // Notify to update Google Play Services.
                    Toast.makeText(activity, "This app requires Google Play Services. Please install " +
                            "Google Play Services on your device and relaunch this app.", Toast.LENGTH_SHORT).show()

                } else {
                    getResultsFromApi(activity)
                }

                return
            }
            REQUEST_ACCOUNT_PICKER -> {

                if (resultCode == Activity.RESULT_OK && data != null && data.extras != null) {

                    //get account name; store in Shared Preferences

                    val accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)

                    accountName?.let { accountName ->
                        val sharedPref =  SharedPrefUtil.init(activity)

                        sharedPref.storeUsernameToPrefs(accountName)

                        getResultsFromApi(activity)
                    }
                }

                return
            }
            REQUEST_AUTHORIZATION -> {
                if (resultCode == Activity.RESULT_OK) {

                    executeAction.invoke()
                    return
                }
            }

        }
    }

}