package com.example.youtubeapp.common.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/** Rae-An Andres created on 2020-06-02*/

open class BaseFragment : Fragment() {

    private lateinit var mFragmentManager: FragmentManager

    private lateinit var mFragmentTransaction: FragmentTransaction

    protected var mPageTitle: String? = null
    protected var mPageCount: Int? = 0

    protected lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    protected fun addFragment(
        fragment: Fragment?, fragmentManager: FragmentManager?,
        id: Int, fragmentTransaction: FragmentTransaction
    ): FragmentTransaction? {

        fragmentManager?.let {
            fragmentManager ->
            try {
                return fragmentManager.beginTransaction()
                    .add(id,fragment!!)
            } catch (stateException: IllegalStateException) {
                Log.e("FragmentManager", "exception: ${stateException.message}")
            }
        }
        return null
    }

    protected fun replaceFragment(
        fragment: Fragment?, fragmentManager: FragmentManager?,
        id: Int, fragmentTransaction: FragmentTransaction
    ): FragmentTransaction? {
        fragmentManager?.let {
                fragmentManager ->
            try {
                return fragmentManager.beginTransaction()
                    .replace(id,fragment!!)
            } catch (stateException: IllegalStateException) {
                Log.e("FragmentManager", "exception: ${stateException.message}")
            }
        }

        return null
    }

}