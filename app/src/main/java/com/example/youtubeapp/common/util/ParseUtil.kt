package com.example.youtubeapp.common.util

import android.text.TextUtils

/** Rae-An Andres created on 2020-06-06*/

object ParseUtil {


    fun parseDuration(duration: String): String? {
        var duration = duration
        duration = if (duration.contains("PT")) duration.replace("PT", "") else duration
        duration = if (duration.contains("S")) duration.replace("S", "") else duration
        duration = if (duration.contains("H")) duration.replace("H", ":") else duration
        duration = if (duration.contains("M")) duration.replace("M", ":") else duration
        val split = duration.split(":").toTypedArray()
        for (i in split.indices) {
            val item = split[i]
            split[i] = if (item.length <= 1) "0$item" else item
        }
        return TextUtils.join(":", split)
    }
}