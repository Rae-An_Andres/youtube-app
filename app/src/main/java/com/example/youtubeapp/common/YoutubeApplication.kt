package com.example.youtubeapp.common

import android.app.Activity
import android.app.Application
import android.content.Intent
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.security.ProviderInstaller

/** Rae-An Andres created on 2020-06-02*/


class YoutubeApplication : Application(), LifecycleObserver, ProviderInstaller.ProviderInstallListener {

    private var retryProviderInstall = false

    companion object {
        val INSTANCE by lazy { YoutubeApplication() }

        const val ERROR_DIALOG_REQUEST_CODE = 1

    }


    override fun onCreate() {
        super.onCreate()


        //Update the security provider when the App is created.
        ProviderInstaller.installIfNeededAsync(this, this)
    }

    fun isRetryProviderInstall(): Boolean {
        return retryProviderInstall
    }

    override fun onProviderInstallFailed(p0: Int, p1: Intent?) {
        val availability = GoogleApiAvailability.getInstance()
        if (availability.isUserResolvableError(p0)) {

            // Recoverable error. Show a dialog prompting the user to
            // install/update/enable Google Play services.

            availability.showErrorDialogFragment(
                INSTANCE.applicationContext as Activity,
                p0,
                ERROR_DIALOG_REQUEST_CODE
            ) { // The user chose not to take the recovery action
                onProviderInstallerNotAvailable()
            }
        } else {
            // Google Play services is not available.
            onProviderInstallerNotAvailable()
        }

    }

    override fun onProviderInstalled() {
        // Provider is up-to-date, app can make secure network calls.
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    fun setRetryProviderInstall(state: Boolean) {
        retryProviderInstall = state
    }


    private fun onProviderInstallerNotAvailable() {
        // This is reached if the provider cannot be updated for some reason.
        // App should consider all HTTP communication to be vulnerable, and take
        // appropriate action.


    }


}