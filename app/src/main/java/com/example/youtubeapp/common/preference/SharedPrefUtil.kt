package com.example.youtubeapp.common.preference

import android.content.Context
import android.content.SharedPreferences
import com.example.youtubeapp.common.APP_NAME
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.responsemodel.currentuserchannel.RelatedPlaylists
import com.google.api.services.youtube.model.Channel
import com.google.api.services.youtube.model.ChannelContentDetails
import com.google.api.services.youtube.model.ChannelListResponse
import com.google.api.services.youtube.model.ChannelSnippet
import com.google.gson.reflect.TypeToken

const val SHARED_USERNAME_TAG = "SHARED_USERNAME_TAG"
const val SHARED_CHANNEL_ID_TAG = "SHARED_CHANNEL_ID_TAG"
const val SHARED_FAVORITE_RELATED_PLAYLIST_TAG = "SHARED_FAVORITE_RELATED_PLAYLIST_TAG"
const val SHARED_LIKED_PLAYLIST_TAG = "SHARED_LIKED_PLAYLIST_TAG"
const val SHARED_RELATED_PLAYLIST_TAG = "SHARED_RELATED_PLAYLIST_TAG"
const val SHARED_SNIPPET_TAG = "SHARED_SNIPPET_TAG"

class SharedPrefUtil {



    companion object {

        private lateinit var sharedPrefManager : SharedPreferences

        fun init(context: Context): SharedPrefUtil{

            sharedPrefManager = context.getSharedPreferences(APP_NAME,Context.MODE_PRIVATE)

            return SharedPrefUtil()

        }
    }

    fun storeUsernameToPrefs(username: String) {
        sharedPrefManager.edit().putString(SHARED_USERNAME_TAG, username).apply()
    }

    fun getUserUserNameFromPrefs() : String? {
      return sharedPrefManager.getString(SHARED_USERNAME_TAG, null)
    }


    fun storeChannelIdToPrefs(channelId : String) {
        sharedPrefManager.edit().putString(SHARED_CHANNEL_ID_TAG,channelId).apply()
    }

    fun getChannelIdFromPrefs(): String? {
        return sharedPrefManager.getString(SHARED_CHANNEL_ID_TAG, null)
    }

    fun storeFavoriteRelatedPlaylistsToShared(favoritePlaylist: String) {
        sharedPrefManager.edit().putString(SHARED_FAVORITE_RELATED_PLAYLIST_TAG, favoritePlaylist).apply()
    }

    fun getFavoriteRelatedPlaylistsFromShared() : String? {
        return sharedPrefManager.getString(SHARED_FAVORITE_RELATED_PLAYLIST_TAG,null)
    }

    fun storeLikedPlaylistToShared(liked: String) {
        sharedPrefManager.edit().putString(SHARED_LIKED_PLAYLIST_TAG, liked).apply()
    }

    fun getLikedPlaylistFromShared() : String?{
        return sharedPrefManager.getString(SHARED_FAVORITE_RELATED_PLAYLIST_TAG, null)
    }

    fun storeRelatedPlaylistToShared(relatedPlaylists: ChannelContentDetails.RelatedPlaylists) {
        val playlist = GsonUtil.getJsonFromObject(relatedPlaylists)

        sharedPrefManager.edit().putString(SHARED_RELATED_PLAYLIST_TAG,playlist).apply()
    }

    fun getRelatedPlaylistFromShared(): ChannelContentDetails.RelatedPlaylists? {

        sharedPrefManager.getString(SHARED_RELATED_PLAYLIST_TAG, null)?.let {
            result ->
            return GsonUtil.getObjectFromJson(result,ChannelContentDetails.RelatedPlaylists::class.java)

        }

        return null
    }



}