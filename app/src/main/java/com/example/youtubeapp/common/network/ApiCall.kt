package com.example.youtubeapp.common.network

import android.app.Activity
import android.widget.Toast
import com.example.youtubeapp.common.APP_NAME
import com.example.youtubeapp.common.REQUEST_AUTHORIZATION
import com.example.youtubeapp.common.preference.SharedPrefUtil
import com.example.youtubeapp.responsemodel.currentuserchannel.UserChannelResponse
import com.example.youtubeapp.responsemodel.currentuserplaylist.CurrentUserPlaylistResponse
import com.example.youtubeapp.responsemodel.itemplaylists.ItemPlaylistsResponse
import com.example.youtubeapp.responsemodel.search.YoutubeSearchResponse
import com.example.youtubeapp.responsemodel.videodetails.VideoDetailsResponse
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.http.HttpResponseException
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.ChannelListResponse
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


/** Rae-An Andres created on 2020-06-03*/

class ApiCall {

    companion object {
        val INSTANCE = ApiCall()

        val networkService = NetworkAdapter.getNetworkService()

        val transport: HttpTransport = AndroidHttp.newCompatibleTransport()
        val jsonFactory: JsonFactory = JacksonFactory.getDefaultInstance()

        val uiScope by lazy {
            CoroutineScope(Dispatchers.Main + Job())
        }

        val ioScope by lazy {
            CoroutineScope(Dispatchers.IO + Job())
        }

        val defaultScope by lazy {
            CoroutineScope(Dispatchers.Default + Job())
        }
    }






    inline fun searchUser(apiKey: String?, channelId: String, part: String, order: String, crossinline showLoading: (Boolean) -> Unit, crossinline data: (YoutubeSearchResponse?) -> Unit) {
        apiKey?.let {

                apiKey ->

            showLoading.invoke(true) // notify to start the progress dialog


            networkService.getChannel(
                apiKey = apiKey,
                channelId = channelId,
                part = part,
                order = order,
                maxResults = 20).enqueue(object : Callback<YoutubeSearchResponse> {
                override fun onFailure(call: Call<YoutubeSearchResponse>?, t: Throwable?) {
                    if (t is Exception) {
                        t.printStackTrace()
                    }

                    showLoading.invoke(false) // notify to end the progress dialog

                }

                override fun onResponse(
                    call: Call<YoutubeSearchResponse>?,
                    response: retrofit2.Response<YoutubeSearchResponse>?
                ) {
                    if (call != null && response != null) {

                        if (call.isExecuted && response.isSuccessful) {
                            data.invoke(response.body())

                            showLoading.invoke(false) // notify to end the progress dialog
                        }
                    }
                }
            })

        }
    }

    inline fun getCurrentUserPlaylist(apiKey: String?,part: String, channelId: String, maxResult: Int, crossinline showLoading: (Boolean) -> Unit, crossinline data: (CurrentUserPlaylistResponse?) -> Unit) {
        apiKey?.let {
            apiKey ->

            showLoading.invoke(true)

            networkService.getCurrentUserPlaylist(apiKey = apiKey, part = part, channelId = channelId, maxResults = maxResult).enqueue(object : Callback<CurrentUserPlaylistResponse> {
                override fun onFailure(call: Call<CurrentUserPlaylistResponse>?, t: Throwable?) {
                   if (t is Exception) {
                       t.printStackTrace()
                   }

                    showLoading.invoke(false)
                }

                override fun onResponse(
                    call: Call<CurrentUserPlaylistResponse>?,
                    response: Response<CurrentUserPlaylistResponse>?
                ) {
                    if (call != null && response != null) {
                        if (call.isExecuted && response.isSuccessful) {
                            data.invoke(response.body())

                            showLoading.invoke(false)
                        }
                    }
                }
            })
        }
    }


    inline fun getItemPlaylists(apiKey: String?, part: String, playlistId: String, maxResult: Int, crossinline showLoading: (Boolean) -> Unit, crossinline data: (ItemPlaylistsResponse?) -> Unit) {

        apiKey?.let { apiKey ->

            showLoading.invoke(true)

            networkService.getPlaylistItems(apiKey = apiKey, part = part, playlistId = playlistId, maxResults = maxResult).enqueue(object : Callback<ItemPlaylistsResponse>{
                override fun onFailure(call: Call<ItemPlaylistsResponse>?, t: Throwable?) {
                    if (t is Exception) {
                        t.printStackTrace()
                    }

                    showLoading.invoke(false)
                }

                override fun onResponse(
                    call: Call<ItemPlaylistsResponse>?,
                    response: Response<ItemPlaylistsResponse>?
                ) {
                    if (call != null && response != null) {
                        if (call.isExecuted && response.isSuccessful) {
                            data.invoke(response.body())

                            showLoading.invoke(false)
                        }
                    }
                }
            })
        }
    }

    inline fun getVideoDetails(apiKey: String?, part: String, videoId: String, crossinline showLoading: (Boolean) -> Unit, crossinline completion: (VideoDetailsResponse?) -> Unit) {

        apiKey?.let { apiKey ->

            showLoading.invoke(true)

            networkService.getVideoDetails(apiKey = apiKey, part = part, videoId = videoId).enqueue(object : Callback<VideoDetailsResponse>{
                override fun onFailure(call: Call<VideoDetailsResponse>?, t: Throwable?) {
                    if (t is Exception) {
                        t.printStackTrace()
                    }

                    showLoading.invoke(false)
                }

                override fun onResponse(
                    call: Call<VideoDetailsResponse>?,
                    response: Response<VideoDetailsResponse>?
                ) {
                    if (call != null && response != null) {
                        if (call.isExecuted && response.isSuccessful) {

                            completion.invoke(response.body())

                            showLoading.invoke(false)
                        }
                    }
                }
            })
        }

    }




   inline fun getChannelList(activity: Activity, credential: GoogleAccountCredential, userName: String, crossinline showLoading: (Boolean) -> Unit, crossinline completion: (ChannelListResponse?) -> Unit) {
       val mService = YouTube.Builder(transport,jsonFactory,credential)
           .setApplicationName(APP_NAME)
           .build()

       showLoading.invoke(true)
       ioScope.launch {

           try {

               val channelListResponse = getUserNameChannels(mService,userName)


               uiScope.launch {

                   showLoading.invoke(false)

                   completion.invoke(channelListResponse)
               }


           } catch (e: Exception) {
               uiScope.launch {

                   showLoading.invoke(false)

                   when (e) {
                       is UserRecoverableAuthIOException -> {
                           // known issue; it will fail for the first time to make way for launching the youtube consent API
                           activity.startActivityForResult(e.intent, REQUEST_AUTHORIZATION)

                       }
                       is IOException -> {
                           Toast.makeText(activity,e.message,Toast.LENGTH_SHORT).show()

                       }
                       is HttpResponseException -> {
                           Toast.makeText(activity,e.message,Toast.LENGTH_SHORT).show()

                       }
                       else -> {
                           e.printStackTrace()

                       }
                   }

               }

           }


       }


   }

    inline fun getUserChannelList(apiKey: String?, part: String, channelId: String, crossinline showLoading: (Boolean) -> Unit, crossinline data: (UserChannelResponse?) -> Unit) {

        apiKey?.let {

            apiKey ->

            showLoading.invoke(true)

            networkService.getAuthorFromChannel(
                apiKey = apiKey,
                part = part,
                channelId = channelId).enqueue(object : Callback<UserChannelResponse> {

                override fun onResponse(
                    call: Call<UserChannelResponse>?,
                    response: Response<UserChannelResponse>?
                ) {
                    if (call != null && response != null) {

                        if (call.isExecuted && response.isSuccessful) {
                            data.invoke(response.body())

                            showLoading.invoke(false) // notify to end the progress dialog
                        }

                    }
                }

                override fun onFailure(call: Call<UserChannelResponse>?, t: Throwable?) {
                    if (t is Exception) {
                        t.printStackTrace()
                    }

                    showLoading.invoke(false)
                }

            })
        }

    }


    inline fun getCurrentUserChannelList(activity: Activity ,apiKey: String?, part: String, mine: Boolean, crossinline showLoading: (Boolean) -> Unit, crossinline data: (UserChannelResponse?) -> Unit) {
        apiKey?.let {


                apiKey ->

            showLoading.invoke(true)

            networkService.getPersonalChannel(
                apiKey = apiKey,
                part = part,
                mine = mine).enqueue(object: Callback<UserChannelResponse> {

                override fun onResponse(
                    call: Call<UserChannelResponse>?,
                    response: Response<UserChannelResponse>?
                ) {
                    if (call != null && response != null) {

                        if (call.isExecuted && response.isSuccessful) {
                            data.invoke(response.body())

                            showLoading.invoke(false) // notify to end the progress dialog
                        }

                        if (call.isExecuted) {

                            when {
                                (response.raw().code() == 401) -> {
                                    Toast.makeText(activity, response.raw().networkResponse().message(), Toast.LENGTH_SHORT).show()
                                }

                            }
                        }

                    }
                }

                override fun onFailure(call: Call<UserChannelResponse>?, t: Throwable?) {

                    showLoading.invoke(false) // notify to end the progress dialog


                    when (t) {
                        is UserRecoverableAuthIOException -> {
                            // known issue; it will fail for the first time to make way for launching the youtube consent API
                            activity.startActivityForResult(t.intent, REQUEST_AUTHORIZATION)

                        }
                        is IOException -> {
                            Toast.makeText(activity,t.message,Toast.LENGTH_SHORT).show()

                        }
                        is HttpResponseException -> {
                            Toast.makeText(activity,t.message,Toast.LENGTH_SHORT).show()

                        }
                        else -> {
                            t!!.printStackTrace()

                        }
                    }


                }
            })
        }
    }




    inline fun getCurrentUserChannels(activity: Activity, credential: GoogleAccountCredential, crossinline showLoading: (Boolean) -> Unit, crossinline completion: (ChannelListResponse?) -> Unit) {

            val mService = YouTube.Builder(transport,jsonFactory,credential)
                .setApplicationName(APP_NAME)
                .build()

        showLoading.invoke(true)


        ioScope.launch {

            try {

                val channelListResponse = getMineChannels(mService)


                channelListResponse?.let {
                    channelListResponse ->

                    val sharedPref = SharedPrefUtil.init(activity)

                    val channel = channelListResponse.items[0]

                    sharedPref.storeChannelIdToPrefs(channel.id)

                    sharedPref.storeRelatedPlaylistToShared(channel.contentDetails.relatedPlaylists)


                }


                uiScope.launch {

                    showLoading.invoke(false)

                    completion.invoke(channelListResponse)
                }


            } catch (e: Exception) {
                uiScope.launch {

                    showLoading.invoke(false)

                    when (e) {
                        is UserRecoverableAuthIOException -> {
                            // known issue; it will fail for the first time to make way for launching the youtube consent API
                            activity.startActivityForResult(e.intent, REQUEST_AUTHORIZATION)

                        }
                        is IOException -> {
                            Toast.makeText(activity,e.message,Toast.LENGTH_SHORT).show()

                        }
                        is HttpResponseException -> {
                            Toast.makeText(activity,e.message,Toast.LENGTH_SHORT).show()

                        }
                        else -> {
                            e.printStackTrace()

                        }
                    }

                }

            }


        }


    }


    @Throws(IOException::class)
    fun getMineChannels(mService: YouTube): ChannelListResponse? {

        return mService.channels()
            .list("snippet,contentDetails,brandingSettings,statistics")
            .setMine(true)
//            .setForUsername("GoogleDevelopers") //TODO ("Remove this")
            .execute()

    }

    @Throws(IOException::class)
    fun getUserNameChannels(mService: YouTube, username: String): ChannelListResponse? {
        return mService.channels()
            .list("snippet,contentDetails,brandingSettings,statistics")
            .setForUsername(username)
            .execute()


    }





}