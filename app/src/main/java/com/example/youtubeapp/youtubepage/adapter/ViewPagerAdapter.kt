package com.example.youtubeapp.youtubepage.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/** Rae-An Andres created on 2020-06-02*/

class ViewPagerAdapter (private val fragmentManager: FragmentManager, private val tabNum: Int) : FragmentPagerAdapter(fragmentManager) {


    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    fun addFragment(fragment: Fragment, title: String){
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    override fun getCount(): Int {
        return tabNum
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> {}
            1 -> {}
            2 -> {}
            else -> {}
        }
        return super.getPageTitle(position)
    }


}