package com.example.youtubeapp.youtubepage

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.example.youtubeapp.R
import com.example.youtubeapp.common.API_KEY
import com.example.youtubeapp.common.network.ApiCall
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.common.view.BaseActivity
import com.example.youtubeapp.responsemodel.currentuserplaylist.CurrentUserPlaylistResponse
import com.example.youtubeapp.youtubepage.fragments.VideoDetailsFragment
import com.example.youtubeapp.youtubepage.fragments.VideoPlaylistFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

/** Rae-An Andres created on 2020-06-02*/

class YoutubePageActivity : BaseActivity(){

    private var receivedObject: String? = null


    companion object {
        const val RECEIVED_OBJECT_TAG = "RECEIVED_OBJECT_TAG"

        val INSTANCE = YoutubePageActivity()

        fun navigate(callingActivity: Activity, receivedObject: String?) {
            val intent = Intent(callingActivity, YoutubePageActivity::class.java).apply {
                putExtra(RECEIVED_OBJECT_TAG, receivedObject)
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            callingActivity.startActivity(intent)
        }

    }


    private lateinit var mBottonNavigationView: BottomNavigationView

    private lateinit var mRelativeLayout: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_youtube_page)

        intent?.extras?.let {
            receivedObject = it.getString(RECEIVED_OBJECT_TAG)     // <- received from login
        }

        val deserialized = GsonUtil.getObjectFromJson(receivedObject, String::class.java, false)

        Log.d("YOUTUBE", "Youtube Deserialized: $deserialized")


        mRelativeLayout = findViewById(R.id.youtube_page_layout)

        decorateProgressLoader(mRelativeLayout)


        mBottonNavigationView = findViewById(R.id.bottom_navigation)


        mBottonNavigationView.setOnNavigationItemSelectedListener {
                item ->

            when (item.itemId) {

                R.id.navigation_home -> {


                    callCurrentUserPlaylist(completion = {
                            currentUserPlaylistResponse ->

                        Log.d("YOUTUBE", "Youtube Response: $currentUserPlaylistResponse")

                        replaceFragment(
                            R.id.content,
                            VideoPlaylistFragment.newInstance(
                                "Home",
                                GsonUtil.getJsonFromObject(currentUserPlaylistResponse, true)
                            )
                        )?.commit()
                    })


                    return@setOnNavigationItemSelectedListener true
                }

                R.id.navigation_dashboard -> {

                    replaceFragment(R.id.content,
                        VideoDetailsFragment.newInstance(0,"Trending"))
                        ?.let { it.commit() }

                    return@setOnNavigationItemSelectedListener true

                }

                R.id.navigation_notifications -> {

                    replaceFragment(R.id.content,
                        VideoDetailsFragment.newInstance(0,"Subscription"))
                        ?.let { it.commit() }

                    return@setOnNavigationItemSelectedListener true

                }

            }
            return@setOnNavigationItemSelectedListener false

        }

        mBottonNavigationView.selectedItemId = R.id.navigation_home


    }




    private inline fun callCurrentUserPlaylist(crossinline completion: (CurrentUserPlaylistResponse) -> Unit) {
        checkPermission(activity = this,
            hasPermission = {
                    hasPermission ->

                if (!hasPermission) {

                    askForPermission(this)

                } else {

                    // execute api
                    ApiCall.INSTANCE.getCurrentUserPlaylist(
                        apiKey = API_KEY,
                        part = "snippet,contentDetails,status,id",
                        channelId = receivedObject?.let { it } ?: "",
                        maxResult = 25,
                        showLoading = {
                                showProgress ->

                            when (showProgress) {
                                true -> {
                                    progressBar.visibility = View.VISIBLE
                                }
                                false -> {
                                    progressBar.visibility = View.GONE
                                }
                            }
                        },
                        data = { data ->

                            data?.let {response ->

                                completion.invoke(response)

                            }

                        }

                    )
                }
            })
    }


}