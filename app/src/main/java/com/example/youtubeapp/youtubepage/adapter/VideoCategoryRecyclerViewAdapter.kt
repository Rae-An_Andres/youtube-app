package com.example.youtubeapp.youtubepage.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.youtubeapp.R
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.responsemodel.currentuserplaylist.ItemsItem
import com.example.youtubeapp.youtubepage.VideoDetailsActivity

/** Rae-An Andres created on 2020-06-02*/

class VideoCategoryRecyclerViewAdapter(private val activity: Activity, private var videoCategoryList: List<ItemsItem>) :
    RecyclerView.Adapter<VideoCategoryRecyclerViewAdapter.VideoCategoryListViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoCategoryListViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.item_youtube_video_category,parent,false)

        return VideoCategoryListViewHolder(view)
    }

    override fun getItemCount(): Int {

       return if (videoCategoryList.isNotEmpty()) videoCategoryList.size else 0

    }

    override fun onBindViewHolder(holderCategory: VideoCategoryListViewHolder, position: Int) {

        holderCategory.videoTitle.text = videoCategoryList[position].snippet.title

        holderCategory.videoCount.text = videoCategoryList[position].contentDetails?.let { details -> "${details.itemCount} videos" }


        Glide.with(activity)
            .load(videoCategoryList[position].snippet.thumbnails.medium.url)
            .into(holderCategory.thumbnail)

        holderCategory.parentView.setOnClickListener {
            Log.d("YOUTUBE", "Selected Item: ${videoCategoryList[position]}")


            val serialized = GsonUtil.getJsonFromObject(videoCategoryList[position],true)

            VideoDetailsActivity.navigate(activity,serialized)


        }

    }

    class VideoCategoryListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val videoCount : TextView = itemView.findViewById(R.id.video_details_author)
        val thumbnail : ImageView = itemView.findViewById(R.id.video_category_thumbnail)
        val videoTitle : TextView = itemView.findViewById(R.id.video_details_title)

        val parentView: CardView = itemView.findViewById(R.id.video_category_parent_view)

    }

}
