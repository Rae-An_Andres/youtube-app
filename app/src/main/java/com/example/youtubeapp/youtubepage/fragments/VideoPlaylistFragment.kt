package com.example.youtubeapp.youtubepage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.youtubeapp.R
import com.example.youtubeapp.common.preference.SharedPrefUtil
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.common.view.BaseFragment
import com.example.youtubeapp.responsemodel.currentuserplaylist.CurrentUserPlaylistResponse
import com.example.youtubeapp.youtubepage.adapter.VideoCategoryRecyclerViewAdapter

/** Rae-An Andres created on 2020-06-02*/

class VideoPlaylistFragment : BaseFragment(){

    private lateinit var mRecyclerView : RecyclerView
    private lateinit var mVideoCategoryRecyclerViewAdapter : VideoCategoryRecyclerViewAdapter


    private var stringData : String? = null

    companion object {
        private const val CLASS_NAME = "VideoPlaylistFragment"
        private const val DATA_TAG = "ReceivedData"

        fun newInstance(title: String, serializedYoutubeData: String): Fragment {
            val videoPlaylistFragment =
                VideoPlaylistFragment()
            val bundle = Bundle()
            bundle.putString(CLASS_NAME,title)
            bundle.putString(DATA_TAG,serializedYoutubeData)
            videoPlaylistFragment.arguments = bundle
            return videoPlaylistFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPageTitle = arguments?.let { it.getString(CLASS_NAME) }
        stringData = arguments?.let { it.getString(DATA_TAG) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.layout_video_category_list_fragment,container,false )

        mRecyclerView = mView.findViewById(R.id.video_category_list_recycler_view)


        return mView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val responseData = GsonUtil.getObjectFromJson(stringData, CurrentUserPlaylistResponse::class.java, true)

        val relatedPlaylists = SharedPrefUtil.init(activity!!).getRelatedPlaylistFromShared()


        relatedPlaylists?.let {

            relatedPlaylists ->


        }


        mVideoCategoryRecyclerViewAdapter = VideoCategoryRecyclerViewAdapter(activity!!, responseData.items)
        mRecyclerView.adapter = mVideoCategoryRecyclerViewAdapter
        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mVideoCategoryRecyclerViewAdapter.notifyDataSetChanged()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



    }
}