package com.example.youtubeapp.youtubepage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.youtubeapp.R
import com.example.youtubeapp.common.view.BaseFragment

/** Rae-An Andres created on 2020-06-02*/

class VideoDetailsFragment : BaseFragment() {

    companion object {
        private const val CLASS_NAME = "VideoDetailsFragment"

        fun newInstance(pageCount: Int?, title: String?): Fragment {
            val trendingFragment =
                VideoDetailsFragment()
            val bundle = Bundle()
            bundle.putInt(CLASS_NAME,pageCount!!)
            bundle.putString(CLASS_NAME,title!!)
            trendingFragment.arguments = bundle
            return trendingFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPageCount = arguments?.let { it.getInt(CLASS_NAME, 0) }
        mPageTitle = arguments?.let { it.getString(CLASS_NAME) }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.layout_video_details,container,false )

        return mView

    }

}