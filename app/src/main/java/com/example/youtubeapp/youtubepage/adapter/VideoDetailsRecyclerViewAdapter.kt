package com.example.youtubeapp.youtubepage.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.youtubeapp.R
import com.example.youtubeapp.common.util.ParseUtil
import com.example.youtubeapp.responsemodel.itemplaylists.ItemsItem

/** Rae-An Andres created on 2020-06-04
 * Manulife IT Delivery Center Asia */

class VideoDetailsRecyclerViewAdapter(private val activity: Activity, private val itemDetails: List<ItemsItem>) :
    RecyclerView.Adapter<VideoDetailsRecyclerViewAdapter.VideoDetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoDetailsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_video_details_playlist,parent, false)

        return VideoDetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (itemDetails.isNotEmpty()) itemDetails.size else 0
    }

    override fun onBindViewHolder(holder: VideoDetailsViewHolder, position: Int) {
        holder.detailsTitle.text = itemDetails[position].snippet.title
        holder.detailsAuthor.text = itemDetails[position].snippet.author // FIXME <- should be author

        holder.duration.text = ParseUtil.parseDuration(itemDetails[position].contentDetails.duration?.let { it } ?: "00:00")


        Glide.with(activity)
            .load(itemDetails[position].snippet.thumbnails.high.url)
            .into(holder.videoThumbnail)
    }

    class VideoDetailsViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {


        val cardView : CardView = itemView.findViewById(R.id.item_video_details_cardview)
        val detailsLayout : RelativeLayout = itemView.findViewById(R.id.video_details_layout)
        val videoThumbnail : ImageView = itemView.findViewById(R.id.video_details_thumbnail)
        val detailsTitle : TextView = itemView.findViewById(R.id.video_details_title)
        val detailsAuthor : TextView = itemView.findViewById(R.id.video_details_author)
        val duration : TextView = itemView.findViewById(R.id.video_duration_tv)
    }


}