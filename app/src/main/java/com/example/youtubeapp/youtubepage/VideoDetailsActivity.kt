package com.example.youtubeapp.youtubepage

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.youtubeapp.R
import com.example.youtubeapp.common.API_KEY
import com.example.youtubeapp.common.network.ApiCall
import com.example.youtubeapp.common.util.GsonUtil
import com.example.youtubeapp.common.util.ParseUtil
import com.example.youtubeapp.common.view.BaseActivity
import com.example.youtubeapp.responsemodel.currentuserplaylist.ItemsItem
import com.example.youtubeapp.youtubepage.adapter.VideoDetailsRecyclerViewAdapter


typealias PlaylistItemsItem = com.example.youtubeapp.responsemodel.itemplaylists.ItemsItem

class VideoDetailsActivity : BaseActivity() {

    private lateinit var mRecyclerView : RecyclerView
    private lateinit var mVideoDetailsAdapter : VideoDetailsRecyclerViewAdapter

    private lateinit var mVideoPlaylistTitleTv : TextView
    private lateinit var mVideoPlaylistAuthor : TextView
    private lateinit var mVideoPlaylistCount : TextView

    companion object {
        fun navigate(callingActivity: Activity, item: String) {
            val intent = Intent(callingActivity, VideoDetailsActivity::class.java).apply {
                putExtra("selectedItem",item)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK

            }
            callingActivity.startActivity(intent)
        }
    }

    private var deserializedItem = ItemsItem()
    private lateinit var relativeLayout : RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_details)

        relativeLayout = findViewById(R.id.video_details_parent_layout)

        mVideoPlaylistTitleTv = findViewById(R.id.video_details_playlist_title_tv)
        mVideoPlaylistAuthor = findViewById(R.id.video_details_playlist_author_tv)
        mVideoPlaylistCount = findViewById(R.id.video_detail_count_tv)


        mRecyclerView = findViewById(R.id.video_details_recyclerview)



        intent?.extras?.let {
            val item = it.getString("selectedItem") ?: ""

            deserializedItem = GsonUtil.getObjectFromJson(item,ItemsItem::class.java,true)

        }


        mVideoPlaylistTitleTv.text = deserializedItem.snippet.title
        mVideoPlaylistAuthor.text = deserializedItem.snippet.channelTitle
        mVideoPlaylistCount.text = deserializedItem.contentDetails?.let { item -> "${item.itemCount} videos" }

        deserializedItem.id
        deserializedItem.snippet.channelId
        deserializedItem.snippet.localized.title
        deserializedItem.snippet.localized.description

        decorateProgressLoader(relativeLayout)

        progressBar.visibility = View.GONE


        //fetch channel id from shared preferences

        val playlistId  = deserializedItem.id

        ApiCall.INSTANCE.getItemPlaylists(
            apiKey = API_KEY,
            part = "contentDetails,snippet,status",
            playlistId = playlistId,
            maxResult = 25,
            showLoading = {
                showLoading ->

                if (showLoading) {
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                }
            },
            data = {

                    itemPlaylistsResponse ->


                itemPlaylistsResponse?.let{

                    playlistResponse ->

                    // initialize recyclerview

                    mVideoDetailsAdapter = VideoDetailsRecyclerViewAdapter(this,playlistResponse.items)
                    mRecyclerView.adapter = mVideoDetailsAdapter
                    mRecyclerView.layoutManager = LinearLayoutManager(this)
                    mVideoDetailsAdapter.notifyDataSetChanged()


                    playlistResponse.items.forEach {
                        item ->

                        // get the video duration
                        ApiCall.INSTANCE.getVideoDetails(
                            API_KEY,
                            "snippet,contentDetails",
                            item.snippet.resourceId.videoId,
                            showLoading = { showProgress ->

                                if (showProgress) {
                                    progressBar.visibility = View.VISIBLE
                                } else {
                                    progressBar.visibility = View.GONE
                                }
                            }, completion = {
                                    videoDetails ->

                                videoDetails?.let {

                                    details ->

                                    item.contentDetails.duration = details.items[0].contentDetails.duration
                                    item.contentDetails.caption = details.items[0].contentDetails.caption


                                    // get the video's author

                                    ApiCall.INSTANCE.getUserChannelList(
                                        apiKey = API_KEY,
                                        part = "snippet,contentDetails,statistics",
                                        channelId = details.items[0].snippet.channelId,
                                        showLoading = { showProgress ->
                                            if (showProgress) {
                                                progressBar.visibility = View.VISIBLE
                                            } else {
                                                progressBar.visibility = View.GONE
                                            }
                                        }, data = {
                                                response ->

                                            response?.let {
                                                    details ->

                                                item.snippet.author = details.items[0].snippet.title

                                                mVideoDetailsAdapter.notifyDataSetChanged()
                                            }

                                        }
                                    )


                                }

                            })



                    }


                }


            })

    }



}
