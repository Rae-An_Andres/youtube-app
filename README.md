# Youtube App


### Development Tooling
##### Android
- Android Studio: 3.5.3
- Android SDK Compiler: API Level 29
- Android SDK Build Tools: 29.0.2
- Android SDK Platform Tools: 28.0.1
- Android Kotlin version: 1.3.72
- minSdkVersion: 23 (demise support of Android 5.0)

##### 3rd party Libraries
- Coroutines (Core/ Android): 1.1.1
- Google Play Services: 18.0.0
- Retrofit 2: 2.0.2
- Gson: 2.8.5
- Glide: 4.11.0
- Google Api Youtube Services: v3-rev183-1.22.0

##### Build System
- Android Plugin for Gradle: 3.2
- Gradle/Gradle Wrapper: 5.4.1

##### Programming Language
- JDK: 1.8
- Kotlin: 1.3.72
<br><br/>

### Cloning the repository
- first time cloning the repository
    ```sh
    git clone --git clone https://Rae-An_Andres@bitbucket.org/Rae-An_Andres/youtube-app.git
    cd YoutubeApp
    git checkout develop
    ```    
<br><br/>

#### Build Credentials:
 - please use the debug.keystore included in this project
 - alias: androiddebugkey
 - kspw:
 - stpw:


### Features
- Google Account Checking
- Current user's channel playlist
    - title
    - number of videos (in every list item)
    - thumbnail
- Current video playlist details of every channel
    - title
    - thumbnail
    - author
    - video duration
<br/>
